module Site
  module Helpers
	def render_partial(template)
	  haml template, partial: false
	end
  end
end