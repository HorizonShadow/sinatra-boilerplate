
module Site
  module Helpers
	#Override the default view folder to look in multiple folders
	#Taken from https://coderwall.com/p/ioanja
	def find_template(views, name, engine, &block)
	  views.each { |v| super(v, name, engine, &block) }
	end
  end
end